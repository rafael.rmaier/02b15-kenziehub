import { useEffect, useState } from 'react';
import axios from "axios";
import { DisplayTechs } from '../../Components/DisplayTechs';
import { Profile } from '../../Components/Profile';
import { Button, Container } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root:{
        backgroundColor: theme.palette.secondary.main,
        width: "80vw",
        height: "100vh",
        border: "2px solid black",
    }
}))

export const Home = ({setIsAuthenticated, history, isAuthenticated}) => {

    const localToken = localStorage.getItem("authToken");
    if(!localToken){
        history.push('/');
    }

    const [techsArray, setTechsArray] = useState([]);
    const classes = useStyles();

    const handleLogout = () => {
        setIsAuthenticated(false);
        window.localStorage.clear();
        history.push('/');
    }
   

    useEffect(() => {
        axios
        .get(`https://kenziehub.me/users/${window.localStorage.getItem("userId")}`)
        .then((res) => {
           
            setTechsArray(res.data.techs);
        })

    }, [techsArray])

    


    return (
        <>
        <Container className={classes.root}>
        <Button onClick={handleLogout}  variant="outlined" style={{float: "right", marginTop: "10px"     }}>Sair</Button>
        <Profile/>
        <DisplayTechs techsArray={techsArray}/>
        {/* <div>{techsArray}</div> */}
        </Container>
        </>
        )
} 