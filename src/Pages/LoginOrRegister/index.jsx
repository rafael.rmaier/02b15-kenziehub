import {FormLogin} from '../../Components/FormLogin';
import {Container, Paper, Button} from "@material-ui/core";
import { useState } from 'react';
import { FormRegisterUser } from '../../Components/FormRegisterUser';
import { makeStyles } from '@material-ui/core/styles';
import Typed  from 'react-typed';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.secondary.main,
        display: "flex",
        width: "55vw",
        height: "75vh",
        justifyContent: "center",
        alignItems: "center",
     
    },
    container:{
        width: "42%",
        display: "flex",
        flexFlow: "column wrap",
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center"
    },
    text:{
        display: "flex",
        alignItems: "center"
    }
}))



export const LoginOrRegister = ({setIsAuthenticated, history, isAuthenticated}) => {
    const localToken = localStorage.getItem("authToken") || "";
    if(localToken){
        history.push('/home');
    }

    
   

    const classes = useStyles();
    const [isRegistered, setIsRegistered] = useState(true);
    const handleIsNotRegistered = () => {
        setIsRegistered(false);
    }

    const handleIsRegistered = () => {
        setIsRegistered(true);
    }
    return (
    <Paper className={classes.root}>
        {isRegistered?(
        <div className={classes.container}>
            <Typed style={{fontSize: "3rem", color:"blue"}} strings={['KenzieHub App']} typeSpeed={[5]}></Typed>
            <br/> 
            <FormLogin setIsAuthenticated={setIsAuthenticated} history={history}/>
            <div className={classes.text}><p>Não possui uma conta?<Button onClick={handleIsNotRegistered}> Cadastre-se</Button></p></div>
        </div>) : (
        <div className={classes.container}>
            <FormRegisterUser setIsRegistered={setIsRegistered}/>
            <div className={classes.text}><p>Já possui uma conta?<Button onClick={handleIsRegistered}> Logar</Button></p></div>
        </div>    
        )}

    </Paper>
    )
} 