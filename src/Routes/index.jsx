
import { Route, Switch, useHistory } from 'react-router-dom';
import { Home } from '../Pages/Home';
import { LoginOrRegister } from '../Pages/LoginOrRegister';
import Fade from '@material-ui/core/Fade';



export const Routes = ({history, setIsAuthenticated, isAuthenticated}) => {

    


    return(
        <Switch>
            
            <Route path ="/home">
                <Fade in={isAuthenticated} timeout={1500}>
                <Home history={history} setIsAuthenticated={setIsAuthenticated} isAuthenticated={isAuthenticated}/>
                </Fade>
            </Route>


            <Route exact path="/">
                <LoginOrRegister history={history} setIsAuthenticated={setIsAuthenticated} isAuthenticated={isAuthenticated}/>
            </Route>

        </Switch>
    )
}