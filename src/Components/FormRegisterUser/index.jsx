import {TextField, Button} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert'
import axios from 'axios';
import { useForm} from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {

    display: "flex",
    flexFlow: "column wrap",
    justifyContent: "center",
    alignItem: "center",
    
    
  },

  forms:{
    width: "45%",
    margin: "10px",
  },
  singleLineForms:{
    flexGrow: "2",
    margin: "10px",
    width: "40rem",
  },
  alertMsg:{
   
    flexGrow: "1",
      // width: "100%",
  },
  inputGroup:{
    display: "flex",
    flexFlow: "row nowrap",
    justifyContent: "space-between",
    flexGrow: "2",

  },
  errorsGroup:{
    display: "flex",
    flexFlow: "row wrap",
    justifyContent: "space-between",
  },
  submitButton:{
    margin: "10px", 
    backgroundColor: "#3f51b5", 
    opacity: "0.9",
    '&:hover':{ 
      background: "#3448b4",
    },
    color: "white",

  },
 
});


export const FormRegisterUser = ({setIsRegistered}) => {

    const classes = useStyles();
    const [spanError, setSpanError] = useState('');

    const loginSchema = yup.object().shape({
        email: yup.string().required("Campo obrigatório").email("Formato inválido"),
        password: yup.string().required("Campo obrigatório"),
        confirmPassword: yup.string().required("Campo obrigatório")
        .oneOf([yup.ref('password')], "Senhas devem ser iguais"),
        name: yup.string().required("Campo obrigatório"),
        bio: yup.string().required("Campo obrigatório"),
        contact: yup.string().required("Campo obrigatório"),
        course_module: yup.string().required("Campo obrigatório")
        
    })

    const onSubmitFunction = (data) => {

        axios
        .post('https://kenziehub.me/users', data)
        
        .catch((err) => {
            if(err.response.status === 401){
                setSpanError("Dados inválidos");
            }
        });

        setIsRegistered(true);
        
    }

    const { register, handleSubmit, formState:{errors}, control} = useForm({
        resolver: yupResolver(loginSchema),
    });

    return (
        <form className={classes.root} onSubmit={handleSubmit(onSubmitFunction)}>

            <TextField className={classes.singleLineForms} variant="outlined" label="Email" {...register("email")} 
            error={errors.email} helperText={errors.email?.message}/>

            <TextField className={classes.singleLineForms} variant="outlined" label="Nome"{...register("name")}
            error={errors.name} helperText={errors.name?.message}/>

          <div className={classes.inputGroup}>  
            

            <TextField className={classes.forms} variant="outlined" label="Senha"{...register("password")} type="password" inputProps={{ minLength: 6 }}
            error={errors.password} helperText={errors.password?.message}/>
            
            <TextField className={classes.forms} variant="outlined" label="Confirme sua senha"{...register("confirmPassword")} type="password" 
            inputProps={{ minLength: 6 }}  error={errors.confirmPassword} helperText={errors.confirmPassword?.message}/>
            
          </div>
   
          <div className={classes.inputGroup}>
            <TextField className={classes.forms} variant="outlined" label="Conte um pouco sobre você"{...register("bio")}
            error={errors.bio} helperText={errors.bio?.message}/>
            

            <TextField className={classes.forms} variant="outlined" label="Contato"{...register("contact")}
            error={errors.contact} helperText={errors.contact?.message}/>
            
          </div>

            <TextField className={classes.singleLineForms} variant="outlined" label="Módulo"{...register("course_module")}
            error={errors.course_module} helperText={errors.course_module?.message}/>
     

            <Button variant="outlined" className={classes.submitButton} type="submit">CADASTRAR</Button>
            {spanError && <Alert severity="error" variant="outlined" className={classes.alertMsg}>{spanError}</Alert>}
        </form>

    )
} 