import { useEffect, useState } from 'react';
import axios from 'axios';
import { Box } from '@material-ui/core'; 
import { makeStyles } from '@material-ui/core/styles';
import defaultPic from "../../Assets/Images/defaultprofilepic.jpg"

const useStyles = makeStyles((theme) => ({
    root:{
        marginTop: "25px",
        display: "flex",
        flexFlow: "row wrap",
        alignItems: "center",
      

    },

    profilePic:{
        maxWidth: "250px",
        maxHeight: "250px",
        borderRadius: "200px",

    },
    textInfo:{
        diplay: "flex",
        flexFlow: "column wrap",
    }
}))


export const Profile = () => {

    const classes = useStyles();

    const [userData, setUserData] = useState();

    const api = axios.create({
        baseURL : "https://kenziehub.me"
    })
    const localToken = localStorage.getItem('authToken') || "";
    const userId = localStorage.getItem('userId') || "";
    api.defaults.headers.authorization = `Bearer ${(localToken)}`;

    useEffect(() => {
        api
        .get(`/users/${userId}`)
        .then((res) => {

            setUserData(res.data);
        })
    }, []);

    return (
        <Box className={classes.root}>
            <figure>
                <img className={classes.profilePic} src={userData?.avatar_url || defaultPic } alt="Profile"></img>
            </figure>
            <div className={classes.textInfo}>
                <h1>{userData?.name}</h1>
                <h2>{userData?.email}</h2>
                <h3>{userData?.course_module}</h3>

            </div>


        </Box>
    )




}