import { Card, Grid, Button, Dialog, Paper, TextField } from "@material-ui/core";
import axios from 'axios';
import { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme) => ({

    card:{
        display: "flex",
        flexFlow: "column wrap",
        // justifyContent: "center",
        // alignContent: "center",
        alignItems: "center",
        
        margin: "10px",
        padding: "10px",
    },
    buttonContainer:{
        display: "flex",
        flexFlow: "row wrap",
        justifyContent:"space-around",
        width: "100%",

    },
    input:{
        margin: "25px",
    },
    button:{
  
        backgroundColor: theme.palette.primary.main,
        '&:hover':{ 
            background: "#3448b4",
          },
        opacity: "0.9",
        color: "white",
    },
    dialog:{
        display: "flex",
        flexFlow: "column wrap"
    }


}))

export const Tech = ({tech}) => {

    

    const classes = useStyles();

    const [status, setStatus] = useState('');


    const [token, setToken] = useState(window.localStorage.getItem("authToken"));
    const handleDelete = (techId) => {
        axios
        .delete(`https://kenziehub.me/users/techs/${techId}`, {headers : {Authorization : `Bearer ${token}`}})
        .then((res) => console.log(res))

    }

    const handleSubmitEdit = (techId) => {
        console.log(token);
        axios
        .put(`https://kenziehub.me/users/techs/${techId}`, { status : status}, {headers : {Authorization : `Bearer ${token}`}})
        .then((res) => console.log(res))

        handleClose();
    } 
   
    const handleOpen = () => {
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
    }

    
    const [open, setOpen] = useState(false);
    


    return(
        <Card className={classes.card}>
            <h2>Tecnologia</h2>
            <h2 style={{color: "#E32BC7"}}>{tech.title}</h2>
            <h3>Status</h3>
            <h3 style={{color: "#E32BC7"}}>{tech.status}</h3>
            <div className={classes.buttonContainer}>
            <Button className={classes.button} variant="outlined" onClick={() => handleDelete(tech.id)}>Deletar</Button>
            <Button className={classes.button} variant="outlined" onClick={handleOpen}>Editar</Button>
            </div>
                <Dialog open={open} onClose={handleClose}>
                    <Paper className={classes.dialog} elevation={3}>
                        <TextField className={classes.input} disabled={true} defaultValue={tech.title} variant="outlined"></TextField>
                        <TextField className={classes.input} label="Status" variant="outlined" onChange={(e) => setStatus(e.target.value)}></TextField>
                        <Button className={classes.button} style={{margin: "25px"}} onClick={() => handleSubmitEdit(tech.id)}>Editar</Button>
                    </Paper>
                </Dialog>
        </Card>

    )
}