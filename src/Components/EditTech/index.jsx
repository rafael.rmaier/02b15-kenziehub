import axios from 'axios';
import { useState } from 'react';
import { TextField, Button, Paper} from '@material-ui/core';


export const EditTech = ({techId}) => {

    const [status, setStatus] = useState('');

    const token = window.localStorage.getItem("authToken")

    const onClickEdit = () => {
        axios
        .put(`https://kenziehub.me/users/techs/${techId}`, { status : status}, {headers : {Authorization : `Bearer${token}`}})
    } 
   


    return (
        <Paper elevation={3}>
            <TextField disabled="true" label="Título" variant="outlined"></TextField>
            <TextField disabled="false" label="Status" variant="outlined" onChange={(e) => setStatus(e.target.value)}></TextField>
            <Button onClick={onClickEdit}>Editar</Button>
        </Paper>

    )
    
}