import { Grid, Button, Dialog, Paper, TextField, Container, Grow } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';

import { useEffect, useState } from 'react';
import { Tech } from '../Tech';
import axios from 'axios';
import AddBoxIcon from '@material-ui/icons/AddBox';
import Fade from '@material-ui/core/Fade';


const useStyles = makeStyles((theme) => ({
    header:{
        display: "flex",
        flexFlow: "row wrap",
        justifyContent: "space-between",
        alignContent: "center",
        alignItems: "center"

    },
    dialog:{
        display: "flex",
        flexFlow: "column wrap",

    },

    input:{
        margin: "25px",
    },

    techsContainer:{
        display: "flex",
        flexFlow: "row wrap",
        boxShadow: "0px -10px 5px 3px rgba(0,0,0,0.45)",
        webkiBoxShadow: "1px -10px 5px 3px rgba(0,0,0,0.45)s",
        mozBoxShadow: "1px -10px 5px 3px rgba(0,0,0,0.45)",
        height: "52vh",
        width: "100%",
        overflowY: "scroll",
       
    },
    button:{
        margin: "25px",
        backgroundColor: theme.palette.primary.main,
        '&:hover':{ 
            background: "#3448b4",
          },
        opacity: "0.9",
        color: "white",
    }
}))


export const DisplayTechs = ({techsArray}) => {

    const [techStatus, setTechStatus] = useState("");
    const [techTitle, setTechTitle] = useState("");
    const [token, setToken] = useState(window.localStorage.getItem("authToken"));

    const classes = useStyles();

    const [openDialog, setOpenDialog] = useState(false);

    const api = axios.create({
        baseURL : "https://kenziehub.me"
    })

    const handleOpen = () => {
        setOpenDialog(true);
    }
    
    const handleClose = () => {
        setOpenDialog(false);
    }
    useEffect(() => {

    },[techsArray]);


    const handleSubmitNewTech = () => {
        const localToken = localStorage.getItem('authToken') || "";
        api.defaults.headers.authorization = `Bearer ${(localToken)}`;

        
        
        api
        .post("/users/techs", {"title" : techTitle, "status" : techStatus})
        .then((res) => console.log(res));

        handleClose();        
        // axios
        // .post(`${api}/users/techs`,{title : techTitle, status : techStatus})
        // .then((res) => console.log(res));
    }

    return(
        
        
        <Grid container spacing={3}>
        <Grid item xs={12} className={classes.header}>
            
            <h2>Minhas tecnologias</h2>
            <Button style={{height: "35px"}} className={classes.button} onClick={handleOpen}>Adicionar tecnologia<AddBoxIcon/></Button>
            <Dialog  open={openDialog} onClose={handleClose}>
                    <Paper className={classes.dialog} elevation={3}>
                        <TextField className={classes.input} label="Tecnologia" variant="outlined" onChange={(e) => setTechTitle(e.target.value)}></TextField>
                        <TextField className={classes.input} label="Status" variant="outlined" onChange={(e) => setTechStatus(e.target.value)}></TextField>
                        <Button variant="outlined" className={classes.button} onClick={handleSubmitNewTech}>Adicionar</Button>
                    </Paper>
                </Dialog>

        </Grid>
        <Container elevation={3} className={classes.techsContainer}>
            {techsArray.map((el) => {
                return (
                    <Grid item xs={3} key={el.id}>
                        <Grow in={true} timeout={5000}>
                        <Tech tech={el} />
                        </Grow>
                    </Grid>
                )
            })}
         </Container>
        </Grid>
       
    )
}