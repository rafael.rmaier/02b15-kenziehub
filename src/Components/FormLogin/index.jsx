import {TextField, Button} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert'
import axios from 'axios';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useState } from 'react';
import { makeStyles, useTheme, ThemeProvider } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        flexFlow: "column wrap",
        width: "100%",
        
     
    },
    input:{
        
        margin: "15px 0px",
    },
    button:{
        backgroundColor: theme.palette.primary.main,
        '&:hover':{
            backgroundColor: "#3D82FA"
        },
        opacity: "0.9",
        color: "white",
    }
}))



export const FormLogin = ({setIsAuthenticated, history}) => {

    const theme = useTheme();

    const classes = useStyles();

    const [spanError, setSpanError] = useState('');

    const loginSchema = yup.object().shape({
        email: yup.string().required("Campo obrigatório").email("Formato inválido"),
        password: yup.string().required("Campo obrigatório"),
    })

    const onSubmitFunction = (data) => {
        axios
        .post('https://kenziehub.me/sessions', data)
        .then((res) => {
            window.localStorage.clear();
            window.localStorage.setItem("authToken", res.data.token);
            window.localStorage.setItem("userId", res.data.user.id);
            setIsAuthenticated(true);
            history.push('/home');
            
        })
        .catch((err) => {
            // if(err.response.status === 401){
            //     setSpanError("Email e/ou senha inválidos");
            // }
        });
        
    }

    const { register, handleSubmit, formState:{errors}} = useForm({
        resolver: yupResolver(loginSchema),
    });

    return (
        <>
        <ThemeProvider theme={theme}>
        <form className={classes.root} onSubmit={handleSubmit(onSubmitFunction)}>
            <TextField className={classes.input} variant="outlined" label="Email" {...register("email")}
            error={errors.email} helperText={errors.email?.message}/>
            <TextField className={classes.input} type="password" variant="outlined" label="Senha" inputProps={{ minLength: 6 }} {...register("password")}
            error={errors.password} helperText={errors.password?.message}/>

            {spanError && <Alert severity="error">{spanError}</Alert>}
            <Button className={classes.button}variant="outlined" type='submit'>Entrar</Button>
        </form>
        </ThemeProvider>
        </>
    )
} 