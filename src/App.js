import './App.css';
import { Routes } from './Routes';
import { useState } from 'react';
import { AppBar, Toolbar,  IconButton, Typography, Button, Container } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";
import { createTheme, ThemeProvider} from "@material-ui/core";
import background from "./Assets/Images/animatedshape.svg";


const theme = createTheme({
  palette:{
    secondary:{
      main: "#E5E5E5",
    },
    primary:{
      main: "#3751FA",
    }
  },
  Button:{
    backgroundColor:  "#3751FA",
        '&:hover':{
            backgroundColor: "#3D82FA"
        },
        opacity: "0.9",
        color: "white",
  }
})

const useStyles = makeStyles((theme) => ({
  root:{
    backgroundImage: `url(${background})`,
    width: "100vw",
    backgroundColor: theme.secondary,
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    margin: "0px",
    padding: "0px",
    maxWidth: "100%",
    maxHeight: "100%",
  },
  loginButton:{

  },

}))

function App() {
  const history = useHistory();

  const classes = useStyles(theme);

  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const onClickLogin = () => {
    history.push('/')
  }

  return (

    <ThemeProvider theme={theme}>

      <Container className={classes.root}>
        <Routes history={history} setIsAuthenticated={setIsAuthenticated} isAuthenticated={isAuthenticated}/>
      </Container>
      

    </ThemeProvider>
  );
}

export default App;
